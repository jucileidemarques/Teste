package br.edu.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class JanelaPrincipal extends JFrame {

	private static final long serialVersionUID = 6832829989494438253L;
	private JButton clienteButton;
	private JButton produtoButton;
	private JButton compraButton;
	private JButton itensCompraButton;
	
	private JLabel menuLabel;
	private JLabel espacoVazio;
	
	private JPanel labelPanel;
	private JPanel botoesPanel;
	
	private Container container;
	
	private JanelaCadastroCliente janelaCadastroCliente = new JanelaCadastroCliente();
	private JanelaCadastroProduto janelaCadastroProduto = new JanelaCadastroProduto();
	private JanelaCadastroCompra  janelaCadastroCompra  = new JanelaCadastroCompra();
	private JanelaCadastroItensCompra janelaCadastroItensCompra  = new JanelaCadastroItensCompra();
	
	public JanelaPrincipal() {
		super("Menu");
		
		menuLabel = new JLabel("Selecione a op��o desejada");
		espacoVazio = new JLabel(" ");
		
		labelPanel = new JPanel(new GridLayout(3,1));
		labelPanel.add(espacoVazio);
		labelPanel.add(menuLabel);
		
		clienteButton = new JButton("Cadastro de Clientes");
		clienteButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				janelaCadastroCliente.setVisible(true);	
			}
			
		});
		
		produtoButton = new JButton("Cadastro de Produto");
		produtoButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				janelaCadastroProduto.setVisible(true);	
			}
			
		});		
		
		compraButton = new JButton("Compra");
		compraButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				janelaCadastroCompra.carregarListaClientes();
				janelaCadastroCompra.setVisible(true);
			}
		});		
		
		itensCompraButton = new JButton("Itens de Compra");
		itensCompraButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				janelaCadastroItensCompra.carregarListaProdutos();
				janelaCadastroItensCompra.carregarListaCompra();
				janelaCadastroItensCompra.setVisible(true);
			}
		});	
		
		botoesPanel = new JPanel(new GridLayout(4,1));
		botoesPanel.setPreferredSize(new Dimension(200,50));
		botoesPanel.add(clienteButton);
		botoesPanel.add(produtoButton);
		botoesPanel.add(compraButton);
		botoesPanel.add(itensCompraButton);
		
		container = getContentPane();
		container.setLayout(new BorderLayout());
		container.add(labelPanel, BorderLayout.CENTER);
		container.add(botoesPanel, BorderLayout.EAST);
		
		setSize(700,200);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
	}
	
	public static void main(String[] args) {
		new JanelaPrincipal();
	}
	
}
