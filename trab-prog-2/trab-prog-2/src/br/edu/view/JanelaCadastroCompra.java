package br.edu.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.edu.repository.BancoDados;

public class JanelaCadastroCompra extends JFrame {

	private static final long serialVersionUID = 6832829989494438253L;
	private JButton inserirButton;
	private JButton apagarButton;
	private JButton atualizarButton;
	private JButton selecionarButton;
	private JButton listarButton;
	private JButton voltarButton;
	
	private JLabel numeroLabel;
	private JTextField numeroTextField;
	
	private JLabel selecaoClienteLabel;
	private JComboBox<String> clienteCombo;
	private JLabel espacoVazio;
	
	private JLabel dataLabel;
	private JTextField dataTextField;
	
	private JLabel obsLabel;
	private JTextField obsTextField;
	
	private JPanel cadastroPanel;
	private JPanel botoesPanel;
	
	private Container container;
	
	private BancoDados db = new BancoDados();
		
	public JanelaCadastroCompra() {
		super("Cadastro de Compras");
		
		dataLabel = new JLabel("Data atual");
		Format formatter = new SimpleDateFormat("dd/MM/yyyy");
		
		dataTextField = new JTextField(formatter.format(new Date()));
		dataTextField.setEditable(false);
		
		espacoVazio = new JLabel(" ");
		
		numeroLabel = new JLabel("N�mero da compra");
		numeroTextField = new JTextField(10);
		
		selecaoClienteLabel = new JLabel("Selecione o Cliente na Lista");
		clienteCombo = new JComboBox<String>();
		
		obsLabel = new JLabel("Observa��es");
		obsTextField = new JTextField(60);
			
		cadastroPanel = new JPanel(new GridLayout(9,1));
		cadastroPanel.add(dataLabel);
		cadastroPanel.add(dataTextField);
		cadastroPanel.add(espacoVazio);
		cadastroPanel.add(numeroLabel);
		cadastroPanel.add(numeroTextField);
		cadastroPanel.add(selecaoClienteLabel);
		cadastroPanel.add(clienteCombo);
		cadastroPanel.add(obsLabel);
		cadastroPanel.add(obsTextField);
		
		inserirButton = new JButton("Inserir");
		inserirButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					clienteCombo.getSelectedItem();
					String[] str = clienteCombo.getSelectedItem().toString().split("-");
					Integer cod = Integer.valueOf(str[0].substring(7).trim());
					
					db.inserirCompra(Integer.parseInt(numeroTextField.getText()), obsTextField.getText(), cod.intValue());
					JOptionPane.showMessageDialog(null,"Compra inserida com sucesso!!");
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Erro, "+e.getMessage());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
			
		});
		
		selecionarButton = new JButton("Selecionar");
		selecionarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {	
				String[] arr = db.listarCompra(Integer.parseInt(numeroTextField.getText()));
				
				obsTextField.setText("");
				obsTextField.setText(arr[0]);
				
				int codCliente = Integer.parseInt(arr[1]);
				
				for (int i = 0; i < clienteCombo.getItemCount(); i++) {
					String[] str = clienteCombo.getItemAt(i).toString().split("-");
					Integer cod = Integer.valueOf(str[0].substring(7).trim());
							
					if (cod == codCliente) {
						clienteCombo.setSelectedIndex(i);
						break;
					}
				}
			}
			
		});
		
		apagarButton = new JButton("Apagar");
		apagarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					db.apagarCompra(Integer.parseInt(numeroTextField.getText()));
					numeroTextField.setText("");
					obsTextField.setText("");
					JOptionPane.showMessageDialog(null,"Compra exclu�da com sucesso!!");
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Erro, "+e.getMessage());
				}
			}
			
		});	
		
		atualizarButton = new JButton("Atualizar");
		atualizarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					db.atualizarCompra(Integer.parseInt(numeroTextField.getText()), obsTextField.getText());
					JOptionPane.showMessageDialog(null,"Compra atualizada com sucesso!!");					
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Erro, "+e.getMessage());
				}
			}
			
		});		
		
		listarButton = new JButton("Listagem");
		listarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new JanelaListagemCompra();
			}
			
		});
		
		voltarButton = new JButton("Voltar");
		voltarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
			
		});		
		
		botoesPanel = new JPanel(new GridLayout(1,6));
		botoesPanel.setPreferredSize(new Dimension(100,50));
		botoesPanel.add(inserirButton);
		botoesPanel.add(selecionarButton);
		botoesPanel.add(apagarButton);
		botoesPanel.add(atualizarButton);
		botoesPanel.add(listarButton);
		botoesPanel.add(voltarButton);
		
		container = getContentPane();
		container.setLayout(new BorderLayout());
		container.add(cadastroPanel, BorderLayout.CENTER);
		container.add(botoesPanel, BorderLayout.SOUTH);
		
		setSize(700,300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(false);
		
	}

	public void carregarListaClientes() {
		clienteCombo.removeAllItems();
				
		for ( String s : db.listarTodos() ) {
			clienteCombo.addItem(s);
		}
		
	}
	
}
