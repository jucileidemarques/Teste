package br.edu.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;

import br.edu.repository.BancoDados;

public class JanelaListagemItensCompra extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4781913830124967421L;
	private JPanel listPanel;
	private JPanel buttonPanel;
	private JButton buttonOk;
	private JList<String> list;
	private ScrollPane scrollPane;
	
	public JanelaListagemItensCompra() {
		super("Listagem de Compras");
		
		DefaultListModel<String> listModel = new DefaultListModel<String>();
		
		String anterior = "";
		BancoDados db = new BancoDados();
		for (String str: db.listarTodosItensCompras()) {
			String[] txt = str.split(";");
			
			if (! txt[0].equals(anterior) ) {
				anterior = txt[0];
				listModel.addElement(txt[0]+" - "+txt[1]+" - "+txt[2]+" - "+txt[3]);
			} 
			
			listModel.addElement("      "+txt[4]+" - "+txt[5]);
		}	
		
		list = new JList<String>(listModel);
		scrollPane = new ScrollPane();
		scrollPane.add(list);
		
		listPanel=new JPanel(new GridLayout(1,1));
		listPanel.add(scrollPane);
		
		buttonOk = new JButton("OK");
		buttonOk.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
			
		});
		buttonPanel=new JPanel(new FlowLayout());
		buttonPanel.add(buttonOk);
		
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(listPanel,BorderLayout.CENTER);
		getContentPane().add(buttonPanel,BorderLayout.SOUTH);
		
		addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent arg0) {
				dispose();
			}
			
		});		
		
		setSize(700,300);
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);	
	}
	
}
