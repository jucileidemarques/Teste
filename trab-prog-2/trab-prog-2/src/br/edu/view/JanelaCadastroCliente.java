package br.edu.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.edu.repository.BancoDados;

public class JanelaCadastroCliente extends JFrame {

	private static final long serialVersionUID = 6832829989494438253L;
	private JButton inserirButton;
	private JButton apagarButton;
	private JButton atualizarButton;
	private JButton selecionarButton;
	private JButton listarButton;
	private JButton voltarButton;
	
	private JLabel codigoLabel;
	private JTextField codigoTextField;
	
	private JLabel nomeLabel;
	private JTextField nomeTextField;
	
	private JPanel cadastroPanel;
	private JPanel botoesPanel;
	
	private Container container;
	
	//Conexao com o banco de dados;
	private BancoDados db = new BancoDados();
		
	public JanelaCadastroCliente() {
		super("Cadastro de Clientes");
		
		codigoLabel = new JLabel("Codigo do Cliente");
		codigoTextField = new JTextField(10);
		
		nomeLabel = new JLabel("Nome do Cliente");
		nomeTextField = new JTextField(30);
		
		cadastroPanel = new JPanel(new GridLayout(8,1));
		cadastroPanel.add(codigoLabel);
		cadastroPanel.add(codigoTextField);
		cadastroPanel.add(nomeLabel);
		cadastroPanel.add(nomeTextField);
		
		
		inserirButton = new JButton("Inserir");
		inserirButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					db.inserir(codigoTextField.getText(), nomeTextField.getText());
					JOptionPane.showMessageDialog(null,"Cliente inserido com sucesso!!");
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Erro, "+e.getMessage());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
			
		});
		
		selecionarButton = new JButton("Selecionar");
		selecionarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				nomeTextField.setText("");
				nomeTextField.setText(db.listar(codigoTextField.getText()));			
			}
			
		});		
		apagarButton = new JButton("Apagar");
		apagarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					db.apagar(codigoTextField.getText());
					codigoTextField.setText("");
					nomeTextField.setText("");
					JOptionPane.showMessageDialog(null,"Cliente exclu�do com sucesso!!");
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Erro, "+e.getMessage());
				}
			}
			
		});		
		atualizarButton = new JButton("Atualizar");
		atualizarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					db.atualizar(codigoTextField.getText(), nomeTextField.getText());
					JOptionPane.showMessageDialog(null,"Cliente atualizado com sucesso!!");					
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Erro, "+e.getMessage());
				}
				
			}
			
		});		
		
		listarButton = new JButton("Listagem");
		listarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new JanelaListagemClientes();
			}
			
		});
		
		voltarButton = new JButton("Voltar");
		voltarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
			
		});		
		
		botoesPanel = new JPanel(new GridLayout(1,6));
		botoesPanel.setPreferredSize(new Dimension(100,50));
		botoesPanel.add(inserirButton);
		botoesPanel.add(selecionarButton);
		botoesPanel.add(apagarButton);
		botoesPanel.add(atualizarButton);
		botoesPanel.add(listarButton);
		botoesPanel.add(voltarButton);
		
		container = getContentPane();
		container.setLayout(new BorderLayout());
		container.add(cadastroPanel, BorderLayout.CENTER);
		container.add(botoesPanel, BorderLayout.SOUTH);
		
		setSize(700,300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(false);
		
	}
	
}
