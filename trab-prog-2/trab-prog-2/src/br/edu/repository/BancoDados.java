package br.edu.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BancoDados {
	
	public void inserir(String codigo,String nome) throws Exception {
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("insert into cliente values(?,?)");
		stmt.setInt(1, Integer.parseInt(codigo));
		stmt.setString(2, nome);
		if (nome.equals("")) {
			throw new Exception("Nome n�o pode ser vazio!!");
		} else {
			stmt.executeUpdate();
		}	
		stmt.close();
		conn.close();
	}
	
	public void inserirProduto(String codigo,String nome) throws Exception {
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("insert into produto values(?,?)");
		stmt.setInt(1, Integer.parseInt(codigo));
		stmt.setString(2, nome);
		if (nome.equals("")) {
			throw new Exception("Nome do produto n�o pode ser vazio!!");
		} else {
			stmt.executeUpdate();
		}	
		stmt.close();
		conn.close();
	}
	
	public void inserirCompra(int numero, String descricao, int codCliente) throws Exception {
		
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("insert into compra values(?,?,?,?)");
		stmt.setInt(1, numero);
		stmt.setDate(2, new java.sql.Date(new Date().getTime()));
		stmt.setString(3, descricao);
		stmt.setInt(4, codCliente);
		
		stmt.executeUpdate();
		
		stmt.close();
		conn.close();
	}
	
	public void inserirItemCompra(int codProduto, int numCompra) throws Exception {
			
			Connection conn = Conexao.getConexao();
			PreparedStatement stmt = conn.prepareStatement("insert into itemcompra values(?,?)");
			stmt.setInt(1, codProduto);
			stmt.setInt(2, numCompra);
			
			stmt.executeUpdate();
			
			stmt.close();
			conn.close();
		}
	
	public void apagar(String codigo) throws SQLException {
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("delete from cliente where codigo=?");
		stmt.setInt(1, Integer.parseInt(codigo));
		stmt.executeUpdate();
		stmt.close();
		conn.close();		
	}
	
	public void apagarProduto(String codigo) throws SQLException {
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("delete from produto where codigo=?");
		stmt.setInt(1, Integer.parseInt(codigo));
		stmt.executeUpdate();
		stmt.close();
		conn.close();		
	}
	
	public void apagarCompra(int numero) throws SQLException {
		Connection conn = Conexao.getConexao();
		
		//Pelas restri��es de integridade do bd � necess�rio excluir os itens de compra vinculado a
		//compra atual
		PreparedStatement stmt1 = conn.prepareStatement("delete from itemcompra where numcompra=?");
		stmt1.setInt(1, numero);
		stmt1.executeUpdate();
		
		//Exclus�o da compra
		PreparedStatement stmt2 = conn.prepareStatement("delete from compra where numero=?");
		stmt2.setInt(1, numero);
		stmt2.executeUpdate();
		
		stmt1.close();
		stmt2.close();
		conn.close();		
	}
	
	public void apagarItemCompra(int numCompra, int codItem) throws SQLException {
		Connection conn = Conexao.getConexao();
		
		PreparedStatement stmt1 = conn.prepareStatement("delete from itemcompra where numcompra=? and coditem=?");
		stmt1.setInt(1, numCompra);
		stmt1.setInt(2, codItem);
		stmt1.executeUpdate();
		
		stmt1.close();
		conn.close();		
	}
	
	public void atualizar(String codigo, String nome) throws SQLException {
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("update cliente set nome=? where codigo=?");
		stmt.setString(1, nome);		
		stmt.setInt(2, Integer.parseInt(codigo));
		stmt.executeUpdate();
		stmt.close();
		conn.close();
	}
	
	public void atualizarProduto(String codigo, String nome) throws SQLException {
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("update produto set nome=? where codigo=?");
		stmt.setString(1, nome);		
		stmt.setInt(2, Integer.parseInt(codigo));
		stmt.executeUpdate();
		stmt.close();
		conn.close();
	}
	
	public void atualizarCompra(int numero, String descricao) throws SQLException {
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("update compra set descricao = ? where numero = ?");
		stmt.setString(1, descricao);
		stmt.setInt(2, numero);
		
		stmt.executeUpdate();
		stmt.close();
		conn.close();
	}
	
	public List<String> listarTodos(){
		List<String> nomeClientes = new ArrayList<String>();
		try {
			Connection conn = Conexao.getConexao();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select * from cliente order by codigo");
			while (rs.next()) {
				nomeClientes.add("C�digo: " + rs.getString(1).toString() + " - Nome: " + rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return nomeClientes;
	}
	
	public List<String> listarTodosProdutos(){
		List<String> nomesProduto = new ArrayList<String>();
		try {
			Connection conn = Conexao.getConexao();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select * from produto order by codigo");
			while (rs.next()) {
				nomesProduto.add("C�digo: " + rs.getString(1).toString() + " - Nome: " + rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return nomesProduto;
	}
	
	public List<String> listarTodasCompras(){
		List<String> nomesCompra = new ArrayList<String>();
		try {
			Connection conn = Conexao.getConexao();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select co.numero, co.data, co.descricao, cl.nome from compra co inner join cliente cl on cl.codigo = co.codclie order by numero");
			while (rs.next()) {
				nomesCompra.add("N�mero: " + rs.getString(1).toString() + " - Data: " + rs.getDate(2).toString() + " - Descricao: " + rs.getString(3).toString() + " - Cliente: " + rs.getString(4).toString());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return nomesCompra;
	}
	
	public List<String> listarTodosItensCompras(){
		List<String> nomesCompra = new ArrayList<String>();
		try {
			Connection conn = Conexao.getConexao();
			Statement stm = conn.createStatement();
			String  sql = new String();
			
			sql += " select co.numero,';', co.data,';', co.descricao,';', cl.nome,';', pdt.codigo,';', pdt.nome ";
			sql += "   from compra co ";
			sql += "  inner join itemcompra ic ";
			sql += "     on ic.numcompra = co.numero ";
			sql += "  inner join cliente cl ";
			sql += "     on cl.codigo = co.codclie ";
			sql += "  inner join produto pdt ";
			sql += "     on pdt.codigo = ic.coditem ";
			
			ResultSet rs = stm.executeQuery(sql);
			while (rs.next()) {
				nomesCompra.add(rs.getString(1).toString() + rs.getString(2).toString() + rs.getDate(3).toString() + rs.getString(4).toString() + rs.getString(5).toString() + rs.getString(6).toString()+ rs.getString(7).toString()+ rs.getString(8).toString()+ rs.getString(9).toString()+ rs.getString(10).toString()+ rs.getString(11).toString());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return nomesCompra;
	}
	
	public String listar(String codigo) {
		String nomeCliente="";
		
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement stmt = conn.prepareStatement("select nome from cliente where codigo=? order by numcompra, coditem");	
			stmt.setInt(1, Integer.parseInt(codigo));
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				nomeCliente = rs.getString(1);
			}
			if (nomeCliente.equals("")){
				throw new Exception("O nome do cliente est� em branco!!");
			}
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("Mensagem: "+e.getMessage());
		}
		return nomeCliente;
	}
		
	public String listarProduto(String codigo) {
		String nomeProduto="";
		
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement stmt = conn.prepareStatement("select nome from produto where codigo=?");	
			stmt.setInt(1, Integer.parseInt(codigo));
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				nomeProduto = rs.getString(1);
			}
			if (nomeProduto.equals("")){
				throw new Exception("O nome do Produto est� em branco!!");
			}
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("Mensagem: "+e.getMessage());
		}
		return nomeProduto;
	}
	
	public String[] listarCompra(int numero) {
		String[] vetString = new String[2];
		
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement stmt = conn.prepareStatement("select descricao, codclie from compra where numero=?");	
			stmt.setInt(1, numero);
			ResultSet rs = stmt.executeQuery();
			
			if (rs.next()) {
				vetString[0] = rs.getString(1);
				vetString[1] = rs.getString(2);
			}
			
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("Mensagem: "+e.getMessage());
		}
		return vetString;
	}
	
}
